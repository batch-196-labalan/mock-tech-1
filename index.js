function countLetter(letter, sentence) {
    let result = 0;


      if( letter.length === 1){
        for( i in sentence){
            if(letter === sentence [i]){
                result +=1
            }
        }
        return result
    } else {
        return undefined
    }

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    
}


function isIsogram(text) {

   text = text.toLowerCase();
    
    for (let i = 0; i < text.length; i++) {
        if (text.indexOf(text[i]) !== text.lastIndexOf(text[i]) ){
            return false;
        }
    }
    return true;


    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    
} 

 const isogram= isIsogram('Machine');
 const notIsogram = isIsogram('Hello');


// console.log(isogram);
// console.log(notIsogram);



function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.


    if( age < 13){
        return undefined //return 'Undefined value!';
    } else if ( (age >= 13 && age <= 21) || age >= 65){
       return String(Math.round((price*.80)*100)/100);
    } else  {
        
        return String(Math.round(price*100)/100);
    }
}




function findHotCategories(items) {

    categories = []

   function stock (item){
    if(item.stocks === 0){
        return item
    }
   }

   hotStocks = items.filter(stock)
   for(x in hotStocks){

    if (categories.indexOf(hotStocks[x].category) === -1){
        categories.push(hotStocks[x].category)
    }

   }

   return hotCategories;


    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

}

function findFlyingVoters(candidateA, candidateB) {

 flyingVoters = []
 for(n in candidateA){
    if(candidateB.indexOf(candidateA[n]) !== -1){
        flyingVoters.push(candidateA[n])
    }
 }

 return flyingVoters;

    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
}


 

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};



